{/* <div class="form-group">
            <label for="email">Электропочта</label>
            <input type="email" class="form-control" id="email" placeholder="Введите свою электропочту">
        </div>
        <div class="form-group">
            <label for="password">Пароль</label>
            <input type="password" class="form-control" id="password" placeholder="Введите пароль">
        </div>
        <div class="form-group form-check">
            <input type="checkbox" class="form-check-input" id="exampleCheck1">
            <label class="form-check-label" for="exampleCheck1">Запомнить меня</label>
        </div>
        <button type="submit" class="btn btn-primary">Вход</button> */}

        
const createDivGroup = (className = 'form-group') => {
    const div = document.createElement('div');
    div.classList.add(className);

    return div;
};
const createLabel = (forText, text, className = '') => {
    const label = document.createElement('label');
    label.innerHTML = text;
    if (className) {
        label.classList.add(className);
    }
    label.setAttribute('for', forText);

    return label;
};

const createBtn = (btnType, className, text) => {
    const btn = document.createElement('button');
    btn.classList.add(className);
    btn.setAttribute('type', btnType);
    btn.innerHTML = text;

    return btn;
}

const createInput = (inputType, className, inputId, inputPlaceholder = '') => {
    const inputElem = document.createElement('input');
    inputElem.setAttribute('type', inputType);
    inputElem.classList.add(className);
    inputElem.setAttribute('id', inputId);
    if (inputPlaceholder) {
        inputElem.setAttribute('placeholder', inputPlaceholder);
    }

    return inputElem;
}

const emailIsValid = (email) => {
    return /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email)
  }

const form = document.getElementById('form');

const emailDivGroup = createDivGroup();
const emailLabel = createLabel('email', 'Электропочта');
emailDivGroup.append(emailLabel);
const emailInput = createInput('email', 'form-control', 'email', 'Введите свою электропочту');
emailDivGroup.append(emailInput);

const passwordDivGroup = createDivGroup();
const passwordLabel = createLabel('password', 'Пароль');
passwordDivGroup.append(passwordLabel);
const passwordInput = createInput('password', 'form-control', 'password', 'Введите пароль');
passwordDivGroup.append(passwordInput);

const checkboxDivGroup = createDivGroup();
checkboxDivGroup.classList.add('form-check');
const checkboxInput = createInput('checkbox', 'form-check-input', 'exampleCheck1');
checkboxDivGroup.append(checkboxInput);
const checkboxLabel = createLabel('exampleCheck1', 'Запомнить меня', 'form-check-label');
checkboxDivGroup.append(checkboxLabel);

const btn = createBtn('submit', 'btn', 'Вход');
btn.classList.add('btn-primary');

form.append(emailDivGroup);
form.append(passwordDivGroup);
form.append(checkboxDivGroup);
form.append(btn);

let isFormValid = false;
const btnSubmit = document.getElementsByClassName('btn-primary')[0];
btnSubmit.onclick = (e => {
    e.preventDefault();

    const emailElem = document.getElementById('email');
    const passwordElem = document.getElementById('password');
    const email = emailElem.value.trim();
    const password = passwordElem.value.trim();

    if (!emailIsValid(email)) {
        emailElem.classList.add('is-invalid');
    } else {
        emailElem.classList.remove('is-invalid');
    }

    if (!password) {
        passwordElem.classList.add('is-invalid');
    } else {
        passwordElem.classList.remove('is-invalid');
    }

    if (emailIsValid(email) && password) {
        const remember = document.getElementById('exampleCheck1').checked;
        console.warn({email, password, remember});
    }
})