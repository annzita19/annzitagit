/**
 * Задача 3.
 *
 * Дописать требуемый код что бы код работал правильно.
 * Необходимо проверить длину строки в переменной string.
 * Если она превосходит maxLength – заменяет конец string на ... таким образом, чтобы её длина стала равна maxLength.
 * В консоль должна вывестись (при необходимости) усечённая строка.
 *
 * Условия:
 * - Переменная string должна обладать типом string;
 * - Переменная maxLength должна обладать типом number.
 */

function truncate(string, maxLength) {
    // РЕШЕНИЕ НАЧАЛО
    // if (...) {
    //     return ...
    // } else {
    //     return ...
    // }
    // РЕШЕНИЕ КОНЕЦ
    if (typeof string !== 'string' || typeof maxLength !== 'number') return 'Invalid parametrs'
    if (string.length > maxLength) {
        return `${string.slice(0, 18)}...`
    } else {
        return string
    }
}

console.log(truncate('Вот, что мне хотелось бы сказать на эту тему:', 21)); // 'Вот, что мне хотел...'