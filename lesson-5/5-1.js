/**
 * Задача 1.
 *
 * Создайте функцию `f`, которая возвращает куб числа, переданного в качестве аргумента.
 *
 * Условия:
 * - Генерировать ошибку, если в качестве аргумента был передан не числовой тип.
 */

// РЕШЕНИЕ
function f2(item) {
    if (typeof item !== 'number') throw new Error('Check input parametr'); 
    return Math.pow(item, 3);
}

console.log(f2(2)); // 8