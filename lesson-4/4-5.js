/**
 * Задача 5.
 *
 * Напишите функцию `createArray`, которая будет создавать массив с заданными значениями.
 * Первым параметром функция принимает значение, которым заполнять массив.
 * А вторым — количество элементов, которое должно быть в массиве.
 *
 * Генерировать ошибки, если:
 * - В качестве первого аргумента были переданы не число, не строка, не объект и не массив;
 * - В качестве второго аргумента был передан не число.
 */

// Решение

function createArray(elem, arrLength) {
    if (typeof elem !== 'string' && typeof elem !== 'number' && typeof elem !== 'object' && !Array.isArray(elem)) throw new Error('Check please first param')
    if (typeof arrLength !== 'number') throw new Error('Should be a Number')
    return new Array(arrLength).fill(elem);
}

const result = createArray('x', 5);

console.log(result); // [ x, x, x, x, x ]

exports.createArray = createArray;