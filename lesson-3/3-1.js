/**
 * Задача 1.
 *
 * Дописать требуемый код что бы код работал правильно.
 * Необходимо преобразовать первый символ переданной строки в заглавный.
 *
 * Условия:
 * - Необходимо проверить что параметр str является строкой
 */

function upperCaseFirst(str) {
    // str ← строка которая в нашем случае равна 'pitter' или ''
    // РЕШЕНИЕ НАЧАЛО
    if (typeof str !== 'string') return `It's not a string`
    // console.log(`${str[0].toUpperCase()}${str.slice(1)}`)
    if (str) return `${str[0].toUpperCase()}${str.slice(1)}`;
    return '';
    // РЕШЕНИЕ КОНЕЦ
}

console.log(upperCaseFirst('pitter')); // Pitter
console.log(upperCaseFirst('cat')); // Pitter
console.log(upperCaseFirst('')); // ''